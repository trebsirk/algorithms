import itertools

def isPal(word):
	if len(word) <= 1:
		return True
	else:
		left = 0
		right = len(word) - 1
		while left < right:
			if word[left] == word[right]:
				left += 1
				right -= 1
			else:
				return False
		return True

def isPal2(word):
	if len(word) <= 1:
		return True
	else:
		if word[0] == word[-1]:
			new_word = word[1:-1]
			return isPal2(new_word)
		else:
			False

def isPal3(word):
	return word == ''.join(reversed(word))

def isPal4(word):
	return word == word[::-1]

def isPal5(word):
	if len(word) <= 1:
		return True
	else:
		for i in range(len(word)/2):
			if word[i] != word[-(i+1)]:
				return False
		return True

if __name__ == '__main__':
	examples = ['mom', 'dad', 'abcba', 'bat', 'ab', 'ba']
	examples = [''.join(a) for a in itertools.permutations(examples, 2)]
	print examples

	for ex in examples:
		if isPal5(ex):
			print ex