
def combs1(n, available, used):
  if len(used) == n:
    yield tuple(used)
  elif len(available) <= 0:
    pass
  else:
    head = available.pop(0)
    used.append(head)
    for c in combs1(n, available[:], used[:]):
      yield c
    used.pop()
    for c in combs1(n, available[:], used[:]):
      yield c

def combs2(n, available, used):
  if len(used) == n:
    yield tuple(used)
  elif len(available) <= 0:
    pass
  else:
    for c in combs2(n, available[1:], used[:]+[available[0]]):
      yield c
    for c in combs2(n, available[1:], used[:]):
      yield c

def combs3(n, available, used):
  if len(used) == n:
    yield tuple(used)
  elif len(available) <= 0:
    pass
  else:
    head = available.pop(0)
    used.append(head)
    for c in combs3(n, available, used):
      yield c
    used.pop(-1)
    for c in combs3(n, available, used):
      yield c
    available.insert(0, head)

if __name__ == '__main__':
  teststring = 'abc'
  k = 3
  print 'testing on: ', teststring
  cs = [s for s in combs1(k, list(teststring), [])]
  print 'my combs = ',cs

  # this is for validation
  import itertools
  combs_from_lib = 
    [i for i in itertools.combinations(list(teststring), k)]
  print 'itertools\' combs = ', combs_from_lib


  if cs == combs_from_lib:
    print 'correct'
  else:
    print 'wrong'
