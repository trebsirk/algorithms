#! /usr/bin/env python

from random import randint
import time
import matplotlib
import matplotlib.pyplot as plt
import numpy as np

def swap(lst, i, j):
	temp = lst[i]
	lst[i] = lst[j]
	lst[j] = temp

def swap_bitwise(lst, i, j):
	lst[i] = lst[i] ^ lst[j]
	lst[j] = lst[i] ^ lst[j]
	lst[i] = lst[i] ^ lst[j]

def shuffle(lst, swap_func, should_print=False):
	for i in xrange(len(lst)):
		j = randint(0, len(lst)-1)
		swap_func(lst, i, j)
		if should_print:
			print i, j, lst

# more or less due to Fisher, Yates, Durstenfeld, and Knuth
def shuffle2(lst, swap_func, should_print=False):
	for i in range(len(lst))[::-1]:
		j = randint(0, i)
		swap_func(lst, i, j)
		if should_print:
			print i, j, lst

def demo(shuffle_func):
	lst = range(10)
	print 'list before:', lst
	print 'shuffling ...'
	shuffle_func(lst, swap, True)
	print '...done'
	print 'list after:', lst

def function_times(fs, maxlen):
	res = []
	for f in fs:
		r = []
		for i in xrange(maxlen):
			data = range(i)
			start = time.time()
			shuffle(data, f)
			end = time.time()
			r.append((i, end-start))
		res.append(r)
	return res

def function_times2(fs, maxlen):
	res = []
	for f in fs:
		r = []
		for i in [1000*i for i in xrange(maxlen)]:
			data = range(i)
			start = time.time()
			shuffle(data, f)
			end = time.time()
			r.append((i, end-start))
		res.append(r)
	return res

def plot_prob():
	tests = 10000
	n_elements = 20
	m = np.array([[0.]*n_elements]*n_elements)
	for k in xrange(tests):
		initial = range(n_elements)
		shuffle2(initial, swap)
		for i in range(len(initial)):
			m[i][initial[i]] += 1
	m = m / tests
	plt.imshow(m)
	plt.colorbar()
	plt.title('Color Map')
	plt.show()
	plt.clf()
	#print m.reshape(-1,).tolist()
	plt.title('Histogram')
	plt.hist(m.reshape(-1,).tolist())
	plt.show()
	plt.clf()

def plot_times():
	functions = [swap, swap_bitwise]
	mlen = 1000
	res = function_times(functions, mlen)
	fig = plt.figure(dpi=90)
	ax = fig.add_subplot(111)
	for r in res:
		ax.plot([rr[0] for rr in r], [rr[1] for rr in r])
	ax.legend(['swap', 'bitwise swap'], 
		loc='upper center', 
		#bbox_to_anchor=(0.5, 1.05),
		ncol=2, fancybox=True, shadow=True)
	#ax.set_yscale('log')
	plt.title('Shuffle function runtimes versus list length')
	plt.xlabel('list length')
	plt.ylabel('time (seconds)')
	plt.show()

if __name__ == '__main__':
	#demo(shuffle)
	#plot_times()
	plot_prob()